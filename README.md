﻿
Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio Version 17
VisualStudioVersion = 17.7.34024.191
MinimumVisualStudioVersion = 10.0.40219.1
Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "Fibonacci sequence", "Fibonacci sequence\Fibonacci sequence.vcxproj", "{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}"
EndProject
Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "Perfect number", "Perfect number\Perfect number.vcxproj", "{BD7D79E0-363D-4595-88E6-616FAF45339B}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|x64 = Debug|x64
		Debug|x86 = Debug|x86
		Release|x64 = Release|x64
		Release|x86 = Release|x86
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Debug|x64.ActiveCfg = Debug|x64
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Debug|x64.Build.0 = Debug|x64
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Debug|x86.ActiveCfg = Debug|Win32
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Debug|x86.Build.0 = Debug|Win32
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Release|x64.ActiveCfg = Release|x64
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Release|x64.Build.0 = Release|x64
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Release|x86.ActiveCfg = Release|Win32
		{EE5201D9-0156-4E94-8D62-0B19B4C9A37D}.Release|x86.Build.0 = Release|Win32
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Debug|x64.ActiveCfg = Debug|x64
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Debug|x64.Build.0 = Debug|x64
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Debug|x86.ActiveCfg = Debug|Win32
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Debug|x86.Build.0 = Debug|Win32
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Release|x64.ActiveCfg = Release|x64
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Release|x64.Build.0 = Release|x64
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Release|x86.ActiveCfg = Release|Win32
		{BD7D79E0-363D-4595-88E6-616FAF45339B}.Release|x86.Build.0 = Release|Win32
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
	GlobalSection(ExtensibilityGlobals) = postSolution
		SolutionGuid = {F390799C-A6EC-44FD-BF07-C7AED92CADB4}
	EndGlobalSection
EndGlobal
